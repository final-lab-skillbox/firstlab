data "yandex_compute_image" "my-ubuntu-2204-1" {
  family = "ubuntu-2204-lts"
}

resource "yandex_compute_instance" "my-vm-1" {
  name        = "test-vm-1"
  platform_id = "standard-v1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my-ubuntu-2204-1.id}"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/cloud_vm.pub")}"
  }
}

resource "yandex_vpc_network" "my-net-1" {
  name = "my-net-1"
}
resource "yandex_vpc_subnet" "my-sn-1" {
  network_id     = yandex_vpc_network.my-net-1.id
  v4_cidr_blocks = ["192.168.1.0/24"]
}

#LoadBalancer
resource "yandex_lb_target_group" "my-lb-tg-1" {
  name      = "my-target-group-1"

  target {
    subnet_id = "${yandex_vpc_subnet.my-sn-1.id}"
    address   = "${yandex_compute_instance.my-vm-1.network_interface.0.ip_address}"
  }
}

resource "yandex_lb_network_load_balancer" "my-nw-lb-1" {
  name = "my-network-load-balancer-1"

  listener {
    name = "my-listener-1"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.my-lb-tg-1.id}"

    healthcheck {
      name = "http"
      http_options {
        port = 80
      }
    }
  }
}

resource "yandex_dns_zone" "main_zone" {
  name        = "my-public-zone"
  description = "A-zone_devopstestinglabs.ru"

  labels = {
    label1 = "label-1-value"
  }

  zone             = "devopstestinglabs.ru."
  public           = true
  private_networks = [yandex_vpc_network.my-net-1.id]
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.main_zone.id
  name    = "devopstestinglabs.ru."
  type    = "A"
  ttl     = 200
  data    = [(yandex_lb_network_load_balancer.my-nw-lb-1.listener[*].external_address_spec[*].address)[0][0]]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.nat_ip_address
}

output "ip_address_loadbalancer" {
  value = yandex_lb_network_load_balancer.my-nw-lb-1.listener.*.external_address_spec[0].*.address[0]
}
